FROM nomadiclabs/opam2 as builder
ARG BRANCH=mainnet

RUN git clone --depth 1 -b $BRANCH https://gitlab.com/tezos/tezos.git

WORKDIR tezos

ENV OPAMYES=true

RUN opam init --bare --disable-sandboxing
RUN make build-deps

RUN ocaml /tezos/_opam/share/ocaml-config/gen_ocaml_config.ml 4.06.1 ocaml
RUN eval $(opam config env) && make
RUN mkdir /_scripts && mkdir /_bin
RUN cp -a scripts/docker/entrypoint.sh /_bin/ && \
  cp -a scripts/docker/entrypoint.inc.sh /_bin/ && \
  cp scripts/alphanet.sh /_scripts/ && \
  cp scripts/alphanet_version /_scripts/ && \
  cp src/bin_client/bash-completion.sh /_scripts/ && \
  cp active_protocol_versions /_scripts/


#FROM arm32v7/debian:testing-slim as builder
#ARG BRANCH=mainnet
#
#USER root
#
#ENV QEMU_EXECVE 1
#COPY qemu-arm-static /usr/bin/qemu-arm-static
#
#RUN apt update
#RUN apt-get -y dist-upgrade
#RUN apt-get -y install opam build-essential musl musl-dev musl-tools git pkg-config m4 libgmp-dev libhidapi-dev libev-dev
#
#RUN adduser --disabled-password --gecos '' tezos
#
#USER tezos
#WORKDIR /home/tezos
#
#RUN git clone --depth 1 -b $BRANCH https://gitlab.com/nomadic-labs/tezos.git
#ENV OPAMYES=true
#ENV HOME=/home/tezos
#RUN opam init --bare --disable-sandboxing
#WORKDIR /home/tezos/tezos
#RUN env
#RUN make build-deps
#
#RUN eval $(opam config env) && make
#
#USER root
#RUN mkdir /_scripts && mkdir /_bin
#RUN cp -a scripts/docker/entrypoint.sh /_bin/ && \
#  cp -a scripts/docker/entrypoint.inc.sh /_bin/ && \
#  cp scripts/alphanet.sh /_scripts/ && \
#  cp scripts/alphanet_version /_scripts/ && \
#  cp src/bin_client/bash-completion.sh /_scripts/ && \
#  cp active_protocol_versions /_scripts/
#
######################### final ###############################
FROM arm32v6/alpine as final

LABEL branch=$BRANCH

#ENV QEMU_EXECVE 1

COPY qemu-arm-static /usr/bin/qemu-arm-static

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN apk update && apk upgrade && \
  apk --no-cache add gmp libev hidapi file ldd && \
  rm -f /var/cache/apk/*

RUN mkdir -p /var/run/tezos/node /var/run/tezos/client

COPY --from=builder /_scripts/* /usr/local/share/tezos/
COPY --from=builder /_bin/* /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-node /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-accuser-00* /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-admin-client /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-baker-00* /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-client /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-endorser-00* /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-node /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-protocol-compiler /usr/local/bin/
COPY --from=builder /home/tezos/tezos/tezos-signer /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

